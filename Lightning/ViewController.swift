//
//  ViewController.swift
//  Lightning
//
//  Created by Michal Černý on 21.03.2021.
//

import UIKit
import RealityKit

class ViewController: UIViewController {
    
    @IBOutlet var arView: ARView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sceneAnchor = try! Experience.loadScene()
        arView.scene.anchors.append(sceneAnchor)
        
        let chair = sceneAnchor.chair as? Entity & HasCollision
        arView.installGestures(for: chair!)
        
        let camera = sceneAnchor.camera as? Entity & HasCollision
        arView.installGestures(for: camera!)
        
        let shoe = sceneAnchor.shoe as? Entity & HasCollision
        arView.installGestures(for: shoe!)
    }
}
